import {ROUTE} from './routes'
import BreedList from '../../containers/BreedList'
import Breed from '../../containers/Breed'
import Favorites from '../../containers/Favorites'
import DogCreator from '../../containers/DogCreator'
import RandomImages from '../../containers/RandomImages'

//App main routing screens
export const globalRoutes = [
  {
    route: ROUTE.INDEX,
    component: BreedList
  },
  {
    route: ROUTE.BREED,
    component: Breed
  },
  {
    route: ROUTE.FAVORITES,
    component: Favorites
  },
  {
    route: ROUTE.CREATOR,
    component: DogCreator
  },
  {
    route: ROUTE.RANDOM_IMAGES,
    component: RandomImages
  }
]
