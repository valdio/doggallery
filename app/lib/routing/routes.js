export const ROUTE = {
  //Global navigation routes
  INDEX: '/',
  BREED: '/breed/:id',
  BREED_TAG: '/breed',
  CREATOR: '/creator/:id',
  CREATOR_TAG: '/creator',
  FAVORITES: '/favorites',
  RANDOM_IMAGES: '/random_images/:breed',
  RANDOM_IMAGES_TAG: '/random_images'
}
