import {createStore} from 'redux'
import {persistStore, persistReducer} from 'redux-persist'
import rootReducer from '../reducers'
import AsyncStorage from '@react-native-community/async-storage'

const persistConfig = {
  key: 'app_storage',
  storage: AsyncStorage,
  timeout: null
}

// import extra middleware for redux store
import storeEnhancer from './storeMiddleware'

// Empty initial state. Add data if need be
let initialState = {}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export default () => {
  let store = createStore(persistedReducer, initialState, storeEnhancer)
  let persistor = persistStore(store)
  return {store, persistor}
};
