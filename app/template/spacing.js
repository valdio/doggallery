export const base = {
  marginVertical: 20
}

export const horizontalSpacing = {
  marginHorizontal: 12
}

export const verticalSpacing = {
  marginVertical: 10
}

export const baseSpacing = {
  ...horizontalSpacing,
  ...verticalSpacing
}


