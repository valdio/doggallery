import * as types from './types'

/**
 * Add to cache created dog images for the specified breed.
 * @param breed
 * @param image Path to file.
 * @returns {Function}
 */
export function saveCreatedImage(breed, image) {
  return (dispatch, getState) => {
    dispatch({type: types.CREATE_DOG_FOR_BREED, breed, image})
  }
}
