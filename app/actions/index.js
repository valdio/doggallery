import * as Breed from './breed'
import * as Favorites from './favorites'
import * as Creator from './creator'

export const ActionCreators = Object.assign({},
  Breed,
  Favorites,
  Creator
)
