import * as types from './types'
import Api from '../lib/api'
import {buildResponse} from '../lib/responseObject'

/**
 * Get a list of all dog breeds.
 * @param callback Provided in case we want to return a response back to the user.
 */
export function getDogBreeds(callback) {
  return (dispatch, getState) => {
    return Api.get(`/breeds/list/alsdsdsl`).then(response => {
        if (response && response.message) {
          dispatch(breedsReceived(response.message))
        } else {
          callback && callback(buildResponse(undefined, response.error || 'Something went wrong'))//default error message in case we dont know what happened
        }
      }
    ).catch((exception) => {
      //handle exception
      callback && callback(buildResponse(undefined, 'Something went wrong'))
    })
  }
}

function breedsReceived(breeds) {
  return {
    type: types.BREED_LIST,
    breeds
  }
}

/**
 * Get list of images for the specified breed.
 * @param breed
 * @param callback
 * @returns {function(*, *): Promise<T>}
 */
export function getBreedImages(breed, callback) {
  return (dispatch, getState) => {
    return Api.get(`/breed/${breed}/images`).then(response => {
        if (response && response.message && response.message.length > 0) {
          dispatch(breedImagesReceived(breed, response.message))
        } else {
          callback && callback(buildResponse(undefined, response.error || 'Something went wrong'))
        }
      }
    ).catch((exception) => {
      callback && callback(buildResponse(undefined, 'Something went wrong'))
    })
  }
}

function breedImagesReceived(breed, images) {
  return {
    type: types.BREED_IMAGES,
    breed, images
  }
}
