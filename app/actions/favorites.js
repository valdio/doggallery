import * as types from './types'

/**
 * Add/remove dog breed from favorites.
 * In case the breed is present in the favorites it will be removed, otherwise it will be added.
 * @param breed
 * @returns {Function}
 */
export function toggleFromFavorites(breed) {
  return (dispatch, getState) => {
    dispatch({type: types.TOGGLE_FAVORITE, breed})
  }
}
