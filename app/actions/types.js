//action type of redux persist. Used to handle state updates on re-hydration.
export const REHYDRATE = 'persist/REHYDRATE';

export const BREED_LIST = 'BREED_LIST';
export const BREED_IMAGES = 'BREED_IMAGES';
export const TOGGLE_FAVORITE = 'TOGGLE_FAVORITE';
export const CREATE_DOG_FOR_BREED = 'CREATE_DOG_FOR_BREED';
