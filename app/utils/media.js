import RNFetchBlob from 'rn-fetch-blob'

export const downloadImage = (imageUrl) => {
  RNFetchBlob
    .config({fileCache: true})
    .fetch('GET', imageUrl).then((res) => {
    console.log('Image saved to ' + res.path())
    if (res.data) {
      alert('download successful')
    } else {
      alert('download failed')
    }
  }).catch((errorMessage, statusCode) => {
    console.log(errorMessage)
  })
}

