/**
 * Capitalize the first character of a string
 * @param text
 * @returns {string}
 */
export const capitalize = (text) => text && text.length > 0 ? `${text.charAt(0).toLocaleUpperCase()}${text.slice(1)}` : ''
