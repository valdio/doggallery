import React, {Component} from 'react'
import {View, SectionList, Text, FlatList} from 'react-native'
import {connect} from 'react-redux'
import styles from './styles/breed'
import Header from '../components/Header'
import {capitalize} from '../utils/textUtils'
import back from '../assets/images/back/back_white.png'
import {COLORS} from '../template'
import ImageCard from '../components/ImageCard'
import {Fab, Icon} from 'native-base'
import {ROUTE} from '../lib/routing/routes'
import {pushRouteToHistory} from '../lib/routing/routing'

class Breed extends Component {
  constructor(props) {
    super(props)
    const breed = this.props.match.params.id
    this.state = {breed}
  }

  componentDidMount() {
    this.props.getBreedImages(this.state.breed, response => response.error && console.log('Handle error', response.error))
  }

  render() {
    const breedImages = this.props.breedImages[this.state.breed] || [] //extract only the opened breed images
    const creatorImages = this.props.creatorImages[this.state.breed] || []//extract only the opened breed images
    const images = creatorImages.length > 0 ? [{title: 'Creator images', data: creatorImages}, //only in case we have creator images add them in a new section
      {title: 'Breed images', data: breedImages}] : [{title: 'Breed images', data: breedImages}]

    return (<View style={styles.container}>
      <Header title={capitalize(this.state.breed)} background={COLORS.HEADER_COLOR}
              leftImg={back} leftOnPress={() => this.props.history.goBack()}/>
      <View style={styles.content}>
        <SectionList
          style={styles.list}
          showsVerticalScrollIndicator={false}
          sections={images}
          keyExtractor={(item, index) => item + index}
          renderItem={this._renderSectionContent}
          renderSectionHeader={({section: {title}}) => <Text style={styles.sectionHeader}>{title}</Text>}
        />
      </View>
      <Fab onPress={() => pushRouteToHistory(`${ROUTE.CREATOR_TAG}/${this.state.breed}`, this.props.history)}>
        <Icon name="ios-menu"/>
      </Fab>
    </View>)
  }

  _renderSectionContent = ({section, index}) => {
    if (index !== 0) {
      return null
    }
    return <FlatList numColumns={2}
                     data={section.data || []}
                     renderItem={({item}) => <ImageCard uri={item}/>}
                     keyExtractor={(item, index) => index.toString()}/>
  }
}

function mapStateToProps(state) {
  return {
    breedImages: state.breed && state.breed.images || {},
    creatorImages: state.creator && state.creator.list || {}
  }
}

export default connect(mapStateToProps)(Breed)
