import React, {Component} from 'react'
import {View, FlatList,Text} from 'react-native'
import {connect} from 'react-redux'
import styles from './styles/breedList'
import BreedItem from '../components/BreedItem'
import Header from '../components/Header'
import {COLORS} from '../template'
import back from '../assets/images/back/back_white.png'

class Favorites extends Component {

  render() {
    const favorites = this.props.favorites
    return (<View style={styles.container}>
      <Header title='Favorites' background={COLORS.HEADER_COLOR}
              leftImg={back} leftOnPress={() => this.props.history.goBack()}/>
      <View style={styles.content}>
        {!favorites || favorites.length===0 &&<Text style={styles.noFavText}>You don't have any favorite breeds!</Text>}
        <FlatList
          showsVerticalScrollIndicator={false}
          data={favorites}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) =>
            <BreedItem breed={item}
                       toggleFromFavorites={breed => this.props.toggleFromFavorites(breed)}
                       isFavorite={true}/>}
        />
      </View>
    </View>)
  }
}

function mapStateToProps(state) {
  return {
    favorites: state.favorites && state.favorites.list || []
  }
}

export default connect(mapStateToProps)(Favorites)
