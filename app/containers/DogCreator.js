import React, {Component} from 'react'
import {View, TouchableOpacity, Text} from 'react-native'
import {connect} from 'react-redux'
import styles from './styles/dogCreator'
import Header from '../components/Header'
import {capitalize} from '../utils/textUtils'
import back from '../assets/images/back/back_white.png'
import {COLORS} from '../template'
import SignatureCapture from 'react-native-signature-capture'

class DogCreator extends Component {
  constructor(props) {
    super(props)
    const breed = this.props.match.params.id
    this.state = {breed}
  }

  render() {
    return (<View style={styles.container}>
      <Header title={`${capitalize(this.state.breed)} creator`} background={COLORS.HEADER_COLOR}
              leftImg={back} leftOnPress={() => this.props.history.goBack()}/>
      <View style={styles.content}>
        <SignatureCapture
          style={styles.signature}
          ref="sign"
          onSaveEvent={this._onSaveEvent}
          onDragEvent={this._onDragEvent}
          saveImageFileInExtStorage={false}
          showNativeButtons={false}
          showTitleLabel={false}
          viewMode={'portrait'}/>

        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.buttonStyle} onPress={() => this.saveSign()}>
            <Text style={styles.buttonText}>Save</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonStyle} onPress={() => this.resetSign()}>
            <Text style={styles.buttonText}>Reset</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>)
  }

  saveSign = () => this.refs['sign'].saveImage()
  resetSign = () => this.refs['sign'].resetImage()
  _onDragEvent = () => console.log('drag')
  _onSaveEvent = (result) => {
    this.props.saveCreatedImage(this.state.breed, result.encoded)
    this.props.history.goBack()//go back to previous screen
  }
}

function mapStateToProps(state) {
  return {}
}

export default connect(mapStateToProps)(DogCreator)
