import React, {Component} from 'react'
import {View, FlatList, Text, TouchableOpacity, Animated} from 'react-native'
import {connect} from 'react-redux'
import styles from './styles/randomImages'
import Header from '../components/Header'
import {COLORS} from '../template'
import back from '../assets/images/back/back_white.png'
import ImageSlideShow from '../components/ImageSlideShow'
import FastImage from 'react-native-fast-image'
import {downloadImage} from '../utils/media'

class RandomImages extends Component {
  constructor(props) {
    super(props)
    const breed = this.props.match.params.breed || 'bouvier'
    const images = this._findMyLuckyDogs(breed)
    this.state = {
      breed,
      selectedImageIndex: 1,
      images,
      fadeInAnimValue: new Animated.Value(0)

    }
  }

  componentDidMount() {
    if (!this.props.breedImages[this.state.breed] || this.props.breedImages[this.state.breed].length === 0) {
      this.props.getBreedImages(this.state.breed, response => response.error && console.log('Handle error', response.error))
    }

    this._applySelectedImageAnimation()
  }

  render() {
    const {images, breed, selectedImageIndex} = this.state

    return (<View style={styles.container}>
      <Header title='Favorite Images' background={COLORS.HEADER_COLOR}
              leftImg={back} leftOnPress={() => this.props.history.goBack()}/>
      <View style={styles.content}>
        <ImageSlideShow
          selectedImageIndex={selectedImageIndex} images={images}
          onImagePress={index => this.setState({selectedImageIndex: index})}/>

        <Animated.View style={[styles.selectedImageContainer, {opacity: this.state.fadeInAnimValue}]}>
          <FastImage
            source={{
              uri: images[selectedImageIndex],
              priority: FastImage.priority.normal
            }}
            resizeMode={FastImage.resizeMode.cover}
            style={styles.selectedImage}
          />
        </Animated.View>

        <TouchableOpacity style={styles.downloadButton} onPress={() => downloadImage(images[selectedImageIndex])}>
          <Text style={styles.downloadButtonText}>Download</Text>
        </TouchableOpacity>
      </View>
    </View>)
  }

  _findMyLuckyDogs = (breed) => {
    const allImages = this.props.breedImages && this.props.breedImages[breed] || []
    const allImagesLength = allImages.length
    if (allImagesLength === 0) {
      return []
    }

    const index1 = Math.floor(Math.random() * allImagesLength)
    const index2 = Math.floor(Math.random() * allImagesLength)
    const index3 = Math.floor(Math.random() * allImagesLength)
    return [allImages[index1], allImages[index2], allImages[index3]]
  }

  _applySelectedImageAnimation = () => {
    Animated.timing(
      this.state.fadeInAnimValue,
      {
        toValue: 1,
        duration: 100
      }
    ).start()
  }
}

function mapStateToProps(state) {
  return {
    breedImages: state.breed && state.breed.images || {}
  }
}

export default connect(mapStateToProps)(RandomImages)
