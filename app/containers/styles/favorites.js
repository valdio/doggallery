import React from 'react'
import {Spacing} from '../../template'

export default {
  container: {
    flex: 1
  },
  content: {
    ...Spacing.verticalSpacing
  }
}
