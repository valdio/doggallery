import React from 'react'
import {Spacing, Typography} from '../../template'

export default {
  container: {
    flex: 1
  },
  content: {
    ...Spacing.verticalSpacing
  },
  noFavText: {
    ...Typography.sectionTitle
  }
}
