import React from 'react'
import {COLORS, Spacing, Typography, Buttons} from '../../template'

export default {
  container: {
    flex: 1,
    backgroundColor: COLORS.CREATOR_BG
  },
  content: {
    flex: 1,
    ...Spacing.verticalSpacing,
    marginTop: 0
  },
  signature: {
    flex: 1,
    flexGrow: 1,
    borderColor: '#000033',
    borderWidth: 1
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  },
  buttonStyle: {
    flex: 1,
    ...Buttons.rounded,
    ...Spacing.baseSpacing,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.HEADER_COLOR

  },
  buttonText: {
    ...Spacing.verticalSpacing,
    ...Typography.text,
    color: COLORS.WHITE
  }
}
