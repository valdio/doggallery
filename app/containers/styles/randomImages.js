import React from 'react'
import {Spacing, COLORS, Buttons, Typography} from '../../template'

export default {
  container: {
    flex: 1
  },
  content: {
    ...Spacing.verticalSpacing,
    flex: 1
  },
  selectedImageContainer: {
    flex: 1
  },
  selectedImage: {
    marginTop: 8,
    flex: 1
  },
  downloadButton: {
    backgroundColor: COLORS.BUTTON_COLOR,
    ...Buttons.rounded,
    ...Spacing.baseSpacing
  },
  downloadButtonText: {
    ...Typography.text,
    ...Spacing.verticalSpacing,
    color: COLORS.WHITE
  }
}
