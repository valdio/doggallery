import React from 'react'
import {Spacing, Typography} from '../../template'

export default {
  container: {
    flex: 1
  },
  content: {
    ...Spacing.verticalSpacing
  },
  list: {
    ...Spacing.horizontalSpacing,
    marginBottom: 30
  },
  sectionHeader: {
    ...Typography.sectionTitle,
    marginBottom: 12
  }
}
