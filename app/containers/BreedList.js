import React, {Component} from 'react'
import {View, FlatList} from 'react-native'
import {connect} from 'react-redux'
import styles from './styles/breedList'
import BreedItem from '../components/BreedItem'
import Header from '../components/Header'
import {COLORS} from '../template'
import white_heart from '../assets/images/heart/white_heart.png'
import {pushRouteToHistory} from '../lib/routing/routing'
import {ROUTE} from '../lib/routing/routes'

class BreedList extends Component {

  componentDidMount() {
    this.props.getDogBreeds()
  }

  render() {
    return (<View style={styles.container}>
      <Header title='Dog breads' background={COLORS.HEADER_COLOR}
              rightImg={white_heart} rightOnPress={() => pushRouteToHistory(ROUTE.FAVORITES, this.props.history)}/>
      <View style={styles.content}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={[...this.props.breeds]}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) =>
            <BreedItem breed={item}
                       toggleFromFavorites={breed => this.props.toggleFromFavorites(breed)}
                       isFavorite={!!this.props.favorites.find(i => i === item)}/>}
        />
      </View>
    </View>)
  }
}

function mapStateToProps(state) {
  return {
    breeds: (state.breed && state.breed.list && state.breed.list.sort((a, b) => a - b)) || [],
    favorites: state.favorites && state.favorites.list || []
  }
}

export default connect(mapStateToProps)(BreedList)
