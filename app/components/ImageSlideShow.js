import React, {Component} from 'react'
import {View, StyleSheet, TouchableOpacity, Animated} from 'react-native'
import FastImage from 'react-native-fast-image'
import {DEVICE} from '../lib/device'

export default class ImageSlideShow extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentDidMount() {

  }


  render() {
    const {images = [], onImagePress, selectedImageIndex} = this.props

    return <View style={styles.container}>
      {images.map((uri, index) =>
        <TouchableOpacity
          onPress={() => onImagePress(index)}
          key={`image_${index}`}
          // style={[styles.imageContainer, (index === Math.floor(images.length / 2) ? {...styles.featuredImageContainer} : {})]}
          style={[styles.imageContainer, (index === selectedImageIndex ? {...styles.featuredImageContainer} : {})]}>
          {<FastImage
                source={{
                  uri: (uri || '').startsWith('http') ? uri : `data:image/webp;base64,${uri}`,
                  priority: FastImage.priority.normal
                }}
                resizeMode={FastImage.resizeMode.cover}
                style={styles.image}
              />}
        </TouchableOpacity>
      )}
    </View>
  }

  // _applyAnimation = () => {
  //
  // }


}


export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  },
  imageContainer: {
    marginHorizontal: 2,
    flexGrow: 2,
    height: DEVICE.WIDTH * .4
  },
  featuredImageContainer: {
    flexGrow: 3,
    height: DEVICE.WIDTH * .5
  },
  image: {
    flex: 1
  }
})
