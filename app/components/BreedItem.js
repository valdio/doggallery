import React from 'react'
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native'
import {Spacing, Typography, COLORS} from '../template'
import {Link} from 'react-router-native'
import {capitalize} from '../utils/textUtils'
import {ROUTE} from '../lib/routing/routes'
import full_heart from '../assets/images/heart/full_heart.png'
import heart from '../assets/images/heart/heart.png'

export default BreedItem = ({breed, isFavorite = false, toggleFromFavorites}) =>
  <Link style={styles.container} underlayColor={COLORS.BACKGROUND} to={`${ROUTE.BREED_TAG}/${breed}`}>
    <View style={styles.content}>
      <Text style={styles.breedText}>{capitalize(breed)}</Text>
      <TouchableOpacity onPress={() => toggleFromFavorites(breed)}>
        <Image style={styles.heart} source={isFavorite ? full_heart : heart}/>
      </TouchableOpacity>
    </View>
  </Link>

export const styles = StyleSheet.create({
  container: {
    ...Spacing.horizontalSpacing,
    marginBottom: 8,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: COLORS.TEXT_COLOR
  },
  content: {
    ...Spacing.verticalSpacing,
    ...Spacing.horizontalSpacing,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  breedText: {
    ...Typography.text,
    alignSelf: 'flex-start'
  },
  heart: {
    height: 26,
    width: 26
  }
})
