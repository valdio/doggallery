import React from 'react'
import {View, StyleSheet} from 'react-native'
import FastImage from 'react-native-fast-image'
import {DEVICE} from '../lib/device'

export default ImageCard = ({uri = ''}) =>
  <View style={styles.container}>
    <FastImage
      source={{
        uri: (uri || '').startsWith('http') ? uri : `data:image/webp;base64,${uri}`,
        priority: FastImage.priority.normal
      }}
      resizeMode={FastImage.resizeMode.cover}
      style={styles.image}/>
  </View>

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 4,
    marginBottom: 8,
    borderRadius: 5,
    overflow: 'hidden'
  },
  image: {
    width: DEVICE.WIDTH * .5,
    height: DEVICE.WIDTH * .4
  }
})
