import createReducer from '../lib/createReducer'
import * as types from '../actions/types'

export const creator = createReducer({}, {
  [types.CREATE_DOG_FOR_BREED](state, action) {
    return {
      ...state,
      list: {
        ...(state.list || {}),
        [action.breed]: [...(state.list && state.list[action.breed] || []), action.image]
      }
    }
  }
})
