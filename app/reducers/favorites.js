import createReducer from '../lib/createReducer'
import * as types from '../actions/types'

export const favorites = createReducer({}, {
  // Can be used to catch the redux re-hydration event and reset the redux cache if needed. Used in development only!
  // [types.REHYDRATE](state, action) {
  //   return {
  //     ...state
  //   }
  // },
  [types.TOGGLE_FAVORITE](state, action) {
    return {
      ...state,
      list: (state.list || []).find(item => item === action.breed) ? state.list.filter(item => item !== action.breed) : [...(state.list || []), action.breed]
    }
  }
})
