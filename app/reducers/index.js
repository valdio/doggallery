import {combineReducers} from 'redux'
import * as breedReducer from './breed'
import * as favoritesReducer from './favorites'
import * as creatorReducer from './creator'

export default combineReducers(Object.assign(
  breedReducer,
  favoritesReducer,
  creatorReducer
))
