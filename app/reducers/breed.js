import createReducer from '../lib/createReducer'
import * as types from '../actions/types'

export const breed = createReducer({}, {
  // Can be used to catch the redux re-hydration event and reset the redux cache if needed. Used in development only!
  // [types.REHYDRATE](state, action) {
  //   return {
  //     ...state
  //   }
  // },
  [types.BREED_LIST](state, action) {
    return {...state, list: Object.keys(action.breeds || {}) || []}//get only the breed keys as an array
  },
  [types.BREED_IMAGES](state, action) {
    return {
      ...state,
      images: {
        ...state.images,
        [action.breed]: action.images
      }

    }
  }
})
