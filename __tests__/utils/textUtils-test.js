import React from 'react'
import {capitalize} from '../../app/utils/textUtils'

test('Capitalize the first character of string', () => {
  expect(capitalize('text')).toBe('Text')
  expect(capitalize('Text')).toBe('Text')
  expect(capitalize('')).toBe('')
  expect(capitalize()).toBe('')
})
