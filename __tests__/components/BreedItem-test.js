import React from 'react'
import renderer from 'react-test-renderer'
import BreedItem from '../../app/components/BreedItem'
import {Text, TouchableOpacity} from 'react-native'
import Enzyme, {shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({adapter: new Adapter()})

describe('BreedItem render tree', () => {
  const tree = renderer.create(<BreedItem breed='dog-breed'/>)
  const testInstance = tree.root
  test('tree exists', () => expect(tree).toBeTruthy())
  test('dog breed is displayed', () => expect(testInstance.findAllByType(Text)).toHaveLength(1))
  test('breed name properly set', () => expect(testInstance.findByType(Text).props.children).toEqual('Dog-breed'))
  test('add to favorites button', () => expect(testInstance.findAllByType(TouchableOpacity)).toHaveLength(1))
})

test('BreedItem event handling', () => {
  const onPressEvent = jest.fn()
  onPressEvent.mockReturnValue('Add to favorites!')
  const wrapper = shallow(<BreedItem breed='dog-breed' toggleFromFavorites={onPressEvent}/>)
  //simulate press 2 times
  wrapper.find(TouchableOpacity).first().props().onPress()
  wrapper.find(TouchableOpacity).first().props().onPress()
  expect(onPressEvent).toHaveBeenCalledTimes(2)//expect to have recorded 2 clicks
  expect(onPressEvent).toHaveBeenCalledWith('dog-breed')//toggleFromFavorites should be called with the dog breed identifier passed in the props
})




