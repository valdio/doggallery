import React from 'react'
import renderer from 'react-test-renderer'
import ImageCard from '../../app/components/ImageCard'
import FastImage from 'react-native-fast-image'

const dummyImageEndpoint = 'https://endpoint/to/image.jpg'

describe('ImageCard render tree', () => {
  const tree = renderer.create(<ImageCard uri={dummyImageEndpoint}/>)
  const testInstance = tree.root
  test('tree exists', () => expect(tree).toBeTruthy())
  test('FastImage is displayed', () => expect(testInstance.findAllByType(FastImage)).toHaveLength(1))
  test('FastImage i loading the correct image', () => expect(testInstance.findByType(FastImage).props.source.uri).toBe(dummyImageEndpoint))
})
