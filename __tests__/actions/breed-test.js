import React from 'react'
import thunkMiddleware from 'redux-thunk'
import configureStore from 'redux-mock-store'
import {getDogBreeds, getBreedImages} from '../../app/actions/breed'
import 'isomorphic-fetch'
import * as types from '../../app/actions/types'

const middlewares = [thunkMiddleware]
const mockStore = configureStore(middlewares)
const testDogBreed = 'akita'

it('should fetch dog breeds', () => {
  const store = mockStore({})
  return store.dispatch(getDogBreeds())
    .then(() => {
        const actions = store.getActions()
        //validate the correct format of the action => {type:types.BREED_LIST,breeds:{}}
        expect(actions[0].type).toBe(types.BREED_LIST)
        expect(actions[0]).toHaveProperty('breeds')
      }
    )
})

it('should fetch dog breeds IMAGES', () => {
  const store = mockStore({})
  return store.dispatch(getBreedImages(testDogBreed))
    .then(() => {
        const actions = store.getActions()
        //validate the correct format of the action => {type:types.BREED_IMAGES,breed:'breed-name',images:[]}
        expect(actions[0].type).toBe(types.BREED_IMAGES)
        expect(actions[0].breed).toBe(testDogBreed)
        expect(actions[0]).toHaveProperty('images')
      }
    )
})


