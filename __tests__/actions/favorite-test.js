import React from 'react'
import configureStore from 'redux-mock-store'
import {toggleFromFavorites} from '../../app/actions/favorites'
import * as types from '../../app/actions/types'
import thunkMiddleware from 'redux-thunk'

const middlewares = [thunkMiddleware]
const mockStore = configureStore(middlewares)
const testDogBreed = 'akita'

it('should add/remove breed from favorites', () => {
  const store = mockStore({})
  store.dispatch(toggleFromFavorites(testDogBreed))
  const actions = store.getActions()
  //validate the correct format of the action => {type:types.TOGGLE_FAVORITE,breed:'breed-name'}
  expect(actions[0].type).toBe(types.TOGGLE_FAVORITE)
  expect(actions[0].breed).toBe(testDogBreed)
})


