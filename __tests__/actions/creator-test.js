import React from 'react'
import configureStore from 'redux-mock-store'
import {saveCreatedImage} from '../../app/actions/creator'
import * as types from '../../app/actions/types'
import thunkMiddleware from 'redux-thunk'

const middlewares = [thunkMiddleware]
const mockStore = configureStore(middlewares)
const testDogBreed = 'akita'
const imageBase64 = 'base 64 string of image'

it('should add a creator image in the redux store', () => {
  const store = mockStore({})
  store.dispatch(saveCreatedImage(testDogBreed, imageBase64))
  const actions = store.getActions()
  //validate the correct format of the action => {type:types.CREATE_DOG_FOR_BREED,breed:'breed-name'}
  expect(actions[0].type).toBe(types.CREATE_DOG_FOR_BREED)
  expect(actions[0].breed).toBe(testDogBreed)
  expect(actions[0].image).toBe(imageBase64)
})
