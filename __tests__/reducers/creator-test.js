import React from 'react'
import {creator} from '../../app/reducers/creator'
import * as types from '../../app/actions/types'

const testDogBreed = 'akita'
const creatorDogImage = 'creator dog image base 64'

describe('Test creator reducer', () => {

  test('Add image to creator redux cache', () => {
    expect(creator({}, {type: types.CREATE_DOG_FOR_BREED, breed: testDogBreed, image: creatorDogImage})).toEqual({
      list: {[testDogBreed]: [creatorDogImage]}
    })
  })

})
