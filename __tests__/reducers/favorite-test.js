import React from 'react'
import {favorites} from '../../app/reducers/favorites'
import * as types from '../../app/actions/types'

const testDogBreed = 'akita'

describe('Test favourites reducer', () => {

  test('Add to favourites', () => {
    expect(favorites({}, {type: types.TOGGLE_FAVORITE, breed: testDogBreed})).toEqual({
      list: [testDogBreed]
    })
  })

  test('Remove fromm favourites', () => {
    expect(favorites({list: [testDogBreed]}, {type: types.TOGGLE_FAVORITE, breed: testDogBreed})).toEqual({
      list: []
    })
  })

})
