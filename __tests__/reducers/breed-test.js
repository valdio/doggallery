import React from 'react'
import {breed} from '../../app/reducers/breed'
import * as types from '../../app/actions/types'

const testDogBreed = 'akita'
const dummyImageEndpoint = 'https://endpoint/to/image.jpg'

describe('Test breed reducer', () => {

  test('Save breeds to redux cache', () => {
    expect(breed({}, {type: types.BREED_LIST, breeds: {'testBreed': {}, 'testBreed1': {}}})).toEqual({
      list: ['testBreed', 'testBreed1']
    })
    expect(breed({}, {type: types.BREED_LIST, breeds: {}})).toEqual({
      list: []
    })
  })

  test('Add breed images to redux cache', () => {
    expect(breed({}, {
      type: types.BREED_IMAGES,
      breed: testDogBreed,
      images: [dummyImageEndpoint, dummyImageEndpoint]
    })).toEqual({
      images: {[testDogBreed]: [dummyImageEndpoint, dummyImageEndpoint]}
    })
  })

})
