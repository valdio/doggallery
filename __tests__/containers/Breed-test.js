import React from 'react'
import renderer from 'react-test-renderer'
import Breed from '../../app/containers/Breed'
import thunkMiddleware from 'redux-thunk'
import configureStore from 'redux-mock-store'
import ImageCard from '../../app/components/ImageCard'
import {Provider} from 'react-redux'
import {FlatList, SectionList} from 'react-native'
import Header from '../../app/components/Header'
import {Fab} from 'native-base'
import Enzyme, {shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

const middlewares = [thunkMiddleware]
const mockStore = configureStore(middlewares)
Enzyme.configure({adapter: new Adapter()})

//helper const
const breedName = 'dog-bread'
const breedImages = {images: {[breedName]: ['image1', 'image2']}}
const creatorImages = {list: {[breedName]: ['creator1', 'creator2', 'creator3']}}
const imageCount = breedImages.images[breedName].length + creatorImages.list[breedName].length

const props = {
  match: {params: {id: breedName}},
  getBreedImages: () => console.log('get breed images')
}

describe('Describe Breed container render tree', () => {
  const store = mockStore({
    breed: breedImages,
    creator: creatorImages
  })
  const tree = renderer.create(<Provider store={store}><Breed  {...props} /></Provider>)
  const testInstance = tree.root

  test('tree exists', () => expect(tree).toBeTruthy())
  test('container has header', () => expect(testInstance.findAllByType(Header)).toHaveLength(1))
  test('displays a SectionList of images', () => expect(testInstance.findAllByType(SectionList)).toHaveLength(1))
  test('content is displaying 2 sections with FlatLists', () => expect(testInstance.findAllByType(FlatList)).toHaveLength(2))
  test('content is the right number of ImageCard', () => expect(testInstance.findAllByType(ImageCard)).toHaveLength(imageCount))
  test('Bread screen should have a button to creator studio', () => expect(testInstance.findAllByType(Fab)).toHaveLength(1))
})
