import React from 'react'
import renderer from 'react-test-renderer'
import BreedList from '../../app/containers/BreedList'
import thunkMiddleware from 'redux-thunk'
import configureStore from 'redux-mock-store'
import BreedItem from '../../app/components/BreedItem'
import {Provider} from 'react-redux'
import {FlatList} from 'react-native'
import Header from '../../app/components/Header'

const middlewares = [thunkMiddleware]
const mockStore = configureStore(middlewares)
const mockBreeds = {list: ['breed1', 'breed2']}
const props = {getDogBreeds: () => console.log('get breed list')}

describe('Describe BreedList render tree', () => {
  const store = mockStore({breed: mockBreeds})
  const tree = renderer.create(<Provider store={store}><BreedList  {...props} /></Provider>)
  const testInstance = tree.root

  test('tree exists', () => expect(tree).toBeTruthy())
  test('container has header', () => expect(testInstance.findAllByType(Header)).toHaveLength(1))
  test('displays a list of dog breeds', () => expect(testInstance.findAllByType(FlatList)).toHaveLength(1))
  test('FlatList is displaying BreedItems', () => expect(testInstance.findAllByType(BreedItem)).toHaveLength(mockBreeds.list.length))
})
