# DogGallery
DogGallery react-native app.

### NOTE! 
Starter React Native project.

React Native app build utilizing the [DOG API](https://dog.ceo/dog-api/). 

#### Used libraries:
* redux
* react-redux
* redux-logger
* redux-thunk
* redux-persist
* react-router-native
* react-native-fast-image - Image caching
* ... others
#### Testing configured with:
* jest
* enzyme
```
#to run tests
npm test
```

## How to run the app?

```
git clone repo

cd ProjectFolder/
```

Start the JS server
```
react-native start --reset-cache
```

Run the app
```
react-native run-android

//or
react-native run-ios
```


Run logs in console
```
react-native log-android

//or
react-native log-ios
```

Build Android APK
```
cd android
./gradlew clean #clean cached android data
./gradlew assembleRelease #build for release
./gradlew assembleDebug #build for debug

#APK generated under folder
# /android/app/build/outputs/apk
```
